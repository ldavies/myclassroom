var fs = require('fs');
var _ = require('underscore')._;
var config = require('./config.json');

function ucfirst(str)
{
    return str.charAt(0).toUpperCase() + str.slice(1);
}

// PersistenceJS is used for the MySQL connection
global.Sequelize = require('sequelize');

switch (config.db.dialect) {
	case 'mysql':
		// MySQL
		global.seqInstance = new Sequelize('myclassroom_dev', config.db.username, config.db.password,
		{
			host: config.db.host,
			port: config.db.port,
			dialect: config.db.dialect,
			pool: { maxConnections: 5, maxIdleTime: 30}
		});
		break;
	case 'sqlite':
	case 'default':
		// SQLite
		global.seqInstance = new Sequelize('myclassroom_dev',config.db.username,config.db.password,
		{
			dialect: config.db.dialect,
			storage: config.db.storage
		});
}


// read all model files automagically.
// blocking call to read directory. No matter,
//  this will only be called once on startup.
var modelfiles = fs.readdirSync(__dirname + '/models/');
var models = {};
_.each(modelfiles,function(e,i) {
	if (e != 'associations.js') {
		// WARNING: the below assigns models to global variables.
		// I do this for convenience, and it is only done once.
		global[ucfirst(e).split(".")[0]] = global.seqInstance.import(__dirname + "/models/" + e.split(".")[0]);
	}
});
// create model associations
require(__dirname+"/models/associations");
seqInstance.sync(); // create new tables if need be.


//exports.Sequelize = Sequelize;
//exports.seqInstance = seqInstance;
