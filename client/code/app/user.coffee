exports.state = require("/state").user.state
exports.userList = new helpers.objectList()
exports.display_name = 'User'
exports.module_name = 'user'

exports.find = (userId) ->
  exports.userList.find({userId: userId})

exports.redraw = (cb) ->
  # No list to redraw for students. Teachers will have a list.
  cb && cb()

exports.save = () ->
  # nothing to save really.

exports.fetch = (callback = false) ->
  ss.rpc "state.fetch", exports.module_name, (success, obj) ->
    if success
      alerts.flash exports.display_name + " state fetched successfully", "notice" # TODO delete these notices
      exports.state = obj # FIXME use a setter for this module. We can trust the server, but it's best for error checking.

      # do we need to refresh the user list?
      if (summary = exports.userList.summary())
        ss.rpc "model.needListRefresh", exports.module_name, summary, (needRefresh) ->
          if needRefresh
            exports.getList(callback)
          else
            callback && callback()
      else
        exports.getRecentMessages(callback)
    else
      alerts.flash exports.display_name + " state not fetched successfully. Error: " + obj
      callback()

exports.getList = (cb = false) ->
  ss.rpc "model.getList", exports.module_name, (success, list) ->
    if success
      exports.userList.items = list
    else
      alerts.flash "Could not retrieve " + exports.display_name + " list"
    cb && cb()
