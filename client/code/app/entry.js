// This file automatically gets called first by SocketStream and must always exist

// Make 'ss' available to all modules and the browser console
window.ss = require('socketstream');

ss.server.on('disconnect', function(){
  console.log('Connection down :-(');
});

ss.server.on('reconnect', function(){
  console.log('Connection back up :-)');
});

ss.server.on('ready', function(){

  // Wait for the DOM to finish loading
  jQuery(function(){

    window.helpers = require('/helpers'); // separate from clientModules so that redraw isn't called on helpers

    // defined in order of redraw
    window.clientModules = [
      'layout',
      'alerts',
      'auth',
      'user',
      'chat',
      'highlightwords'
    ];
    
    // Load app
    $.each(window.clientModules,function(i,e) {
      window[e] = require('/'+e);
    });

    console.log("On ready redraw.");
    window.helpers.redrawWindow(); // force redraw for now
  });

});
