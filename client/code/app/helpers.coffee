# Private functions
exports.timestamp = (d = new Date()) ->
  d.getHours() + ":" + exports.pad2(d.getMinutes()) + ":" + exports.pad2(d.getSeconds())

exports.pad2 = (number) ->
  ((if number < 10 then "0" else "")) + number

# the following two save and redraw helpers
# are suffixed with Window to avoid recursuion
# from calling save() and redraw() on this module
# FIXME don't ever call this!
exports.saveWindow = () ->
  $.each clientModules, (i,e) ->
    window[e].save()


exports.redrawWindow = (i=0) ->
  # recursively redraw elements in the window in the order specfied in clientModules.
  if i < clientModules.length
    console.log "Redrawing " + clientModules[i]
    window[clientModules[i]].redraw ->
      exports.redrawWindow i+1
  else
    console.log "Finished window redraw"


exports.save = () ->
  # helpers don't really have a state to save


exports.redraw = () ->
  # helpers don't really have a state to redraw

# time for some globals
# TODO use backbone.js. The below is just ridiculous.
exports.objectList = () ->
  this.items = new Array() # Index is unrelated to ID of object. Assuming Sorted Date Ascending.
  this.latest = () ->
    return this.items[this.items.length-1]
  this.summary = () ->
    return $.map this.items, (e,i) ->
      e.id
  this.update = (items) ->
    this.items = items # there is probably a better way of doing this
  this.add = (item) ->
    this.items.push(item)
  this.find = (conditions) ->
    result = false
    $.each this.items, (i,e) ->
      $.each conditions, (key, value) ->
        if e[key] == value then result = e
    result
  this
