exports.state = require("/state").auth.state


exports.redraw = (cb) ->
  # should this call checkAuthenticated??
  if exports.state.authenticated
    exports.showLogoutButton()
    $("#login").remove()
  else
    exports.showLoginWindow()
    $("#logout").remove()
  cb && cb()


exports.save = () ->
  # maybe this is a generic call
  # FIXME don't hard-code module name here - somehow abstract it?
  ss.rpc "state.save", "auth", exports.state, (success) ->
    if success
      console.log("Successfully saved auth state")
    else
      console.log("Could not save auth state")
  


ss.event.on "logout", () ->
  alerts.flash "You have been logged out by the server"
  exports.logout()


exports.showLoginWindow = () ->
  html = ss.tmpl["auth-login"].render()
  # add this to the main pane
  $(html).hide().appendTo($("#layout-cell-center").empty()).fadeIn(500)

exports.showLogoutButton = () ->
  html = ss.tmpl["auth-logout"].render()
  # add this to the main pane
  $(html).hide().appendTo("#layout-cell-north").fadeIn(500)


exports.checkAuthenticated = () ->
  ss.rpc "auth.isAuthenticated", (authenticated, user=false) ->
    if authenticated
      setAuthUser(user)
      # add the user to the current session
      alerts.flash "Welcome, " + user.displayName + ".", "notice"
    else
      purgeAuthUser()
      alerts.flash "You are not logged in."
    helpers.redrawWindow()



exports.authenticate = (username, password) ->
  ss.rpc "auth.authenticate", username, password, (success, user) ->
    if success
      # anything else we want to do. This will essentiall make the app resume normal operation.
      # perhaps load the teacher's default settings or the current presentation
    
      # Assume student login for now. FIXME cater for all logins. The following will be layout dependent.
      #   For now, manually enable some widgets. FIXME the state of all
      #   widgets should all be sent from the teacher via the server.
      # send a request to the server to create the chat winow

      setAuthUser(user)
      alerts.flash "Welcome, " + user.displayName + ". You are now logged in to myClassroom.", "notice"
      helpers.redrawWindow()
    else
      purgeAuthUser()
      alerts.flash "Sorry, your username and/or password did not match any in our system"


exports.logout = () ->
  ss.rpc "auth.logout", (success) ->
    if success
      alerts.flash "You have been successfully logged out.", "notice"
      purgeAuthUser()
    else
      alerts.flash "There was an error in logging out. Please contact your administrator."
    helpers.redrawWindow()



$("#login").live "submit", ->
  
  # Grab the message from the text box
  username = $("#username").val()
  password = $("#password").val()
  
  # Call the 'send' funtion (below) to ensure it's valid before sending to the server
  exports.authenticate username, password
      


$("#logout").live "click", ->
  exports.logout()


# LOCAL helper methods that are inaccessible outside this module.
setAuthUser = (user) ->
  exports.state.authenticated = true
  exports.state.user = user
  # TODO get the study group and store it here
  exports.state.studyGroup = user.studyGroup


purgeAuthUser = () ->
  exports.state.authenticated = false
  exports.state.user = false
  exports.state.studyGroup = false
  



# On startup i.e. as this is executed, check login. If not, show the login window.
exports.checkAuthenticated()
