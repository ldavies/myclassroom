# SchoolChat(TM) is adapted from the SocketStream Quickchat demo

# store all state variables for the chat in here
exports.state = require("/state").chat.state


exports.messageList = new helpers.objectList()


exports.redraw = (cb2) ->
  cb = () ->
    if exports.state.display
      exports.createChat()
      # TODO use user displayname or studygroup name
      if exports.state.enabled then exports.enableChat("Welcome to chat.")
      else exports.disableChat("Chat is offline")
    else exports.removeChat()
    if cb2 then cb2()
  exports.fetch(cb)


exports.save = () ->
  # save the state DEPRECIATED 

exports.getRecentMessages = (cb = false) ->
  ss.rpc "chat.getRecentMessages", (success, messageList) ->
    if success and messageList
      alerts.flash "Chat recent messages fetched successfully", "notice"
      exports.messageList.update messageList
    else
      alerts.flash "Chat recent messages not fetched successfully"
    cb && cb()


exports.fetch = (callback = false) ->
  ss.rpc "state.fetch", "chat", (success, obj) ->
    if success
      alerts.flash "Chat state fetched successfully", "notice" # TODO delete these notices
      exports.state = obj # FIXME use a setter for this module. We can trust the server, but it's best for error checking.
      exports.state.dirty = false

      # do we need to refresh the message log?
      if (latest = exports.messageList.latest())
        ss.rpc "chat.needMessageRefresh", latest, (needRefresh) ->
          if needRefresh
            exports.getRecentMessages(callback)
          else callback && callback()
      else
        exports.getRecentMessages(callback)
    else
      alerts.flash "Chat state not fetched successfully. Error: " + obj
      callback()
    

# Listen out for newMessage events coming from the server
ss.event.on "newMessage", (chatmessage) ->
  # Append it to the messageList
  exports.messageList.add(chatmessage)
  exports.createMessage(chatmessage).appendTo("#chatlog").slideDown()

exports.createMessage = (chatmessage) ->
  # Example of using the Hogan Template in client/templates/chat/message.jade to generate HTML for each message
  html = ss.tmpl["chat-message"].render(
    user:      user.find(chatmessage.userId).name
    message:   chatmessage.body
    activated: chatmessage.activated
    time:      helpers.timestamp(new Date(chatmessage.createdAt))
  ) # TODO change template to use same column names as chatmessage table
  
  # Append it to the #chatlog div and show effect
  $(html).hide()

ss.event.on "createChat", () ->
  # perhaps check if chat is already displayed TODO
  exports.createChat()

ss.event.on "removeChat", () ->
  # perhaps check if chat is already displayed TODO
  exports.removeChat()

ss.event.on "enableChat", (statusMessage) ->
  exports.enableChat(statusMessage)

ss.event.on "disableChat", (statusMessage) ->
  exports.disableChat(statusMessage)

# the following four methods
# create, delete, enable, disable
# are called by on-events from the server
# and are just focused on making the client side
# reflect the state of the server
exports.createChat = (cb) ->
  # destroy the old chat window
  exports.removeChat()
  html = ss.tmpl["chat-interface"].render()

  # TODO make this append to wherever the teacher specifies
  c = $(html).hide().appendTo("#layout-cell-east")

  # Create messages
  $.each exports.messageList.items, (i,e) ->
    console.log(e)
    exports.createMessage(e).appendTo('#chatlog').show()

  c.fadeIn(500)

  # dumb to current state, but we set state.display to true because we know it's now true
  exports.state.display = true
  cb && cb()


exports.removeChat = () ->
  $("#chat").fadeOut(500).remove()
  exports.state.display = false


exports.disableChat = (statusMessage) ->
  if (exports.state.display)
    $("#chatroom-subtitle").html(exports.state.subtitle = statusMessage)
    $("#myMessage").prop('disabled', true)
    exports.state.enabled = false
    # TODO also disable receipt of chat messages from anyone in the studygroup
  else
    throw "Error: Cannot disable chat because chat.state.display is not true."

  
exports.enableChat = (statusMessage) ->
  if (exports.state.display)
    $("#myMessage").prop('disabled', false)
    exports.state.enabled = true
  else
    throw "Error: Cannot enable chat because chat.state.display is not true."


# This is called when the user tries to send a message but is not in a Studygroup
# FIXME possibly redundant
ss.event.on "notInStudygroup", () ->
 alerts.flash "Your message could not be send because you are not in a studygroup."


# Show the chat form and bind to the submit action
$("#chat").live "submit", ->
  
  # Grab the message from the text box
  text = $("#myMessage").val()
  
  # Call the 'send' funtion (below) to ensure it's valid before sending to the server
  exports.sendMessage text, (success, error = "Unable to send message") ->
    if success
      $("#myMessage").val ""
    else
      alerts.flash error



# Demonstrates sharing code between modules by exporting function
exports.sendMessage = (text, cb) ->
  if valid(text)
    ss.rpc "chat.sendMessage", text, cb
  else
    cb false

exports.setFullscreen = (isFullscreen, cb) ->
  ss.rpc "layout.setFullscreen", isFullscreen, cb


valid = (text) ->
  text and text.length > 0

