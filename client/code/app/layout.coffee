exports.state = require("/state").layout.state


exports.setStudentLayout = ->
  layout = ->
    container.layout
      type: "border"
      resize: false
      hgap: 8
      vgap: 8

  html = ss.tmpl["layout-student"].render()
  $("body").html html
  container = $("#content")
  west = $("#layout-cell-west")
  east = $("#layout-cell-east")
  centre = $("#layout-cell-centre")

  
  #	,items: $("div[id^='layout-cell-']")
  $.each
    east: "w"
    west: "e"
  , (i, e) ->
    $("#layout-cell-" + i).resizable
      handles: e
      stop: layout
      helper: "ui-resizable-helper-" + i


  $(window).resize layout
  layout()
  
  # make fullscreen
  $(document).fullScreen true
  $(document).bind "fullscreenchange", ->
    
    # send fullscreen change message to server
    console.log (if "Client Set Fullscreen " + $(document).fullScreen() then "on" else "off")
    setFullscreen $(document).fullScreen(), (cbval) ->
      console.log (if "Callback Fullscreen " + $(document).fullScreen() then "on" else "off")

    true

  exports.state.dirty = false # so we don't redraw ever FIXME make this reflect reality

# end setStudentLayout function
exports.redraw = (cb) ->
  # for now, just call the above if need be
  exports.setStudentLayout()  #if exports.state.dirty
  cb && cb()

exports.save = ->

  # TODO store the position of all frames and other
  # odds and sods in the state object for this module
  # and then have a server rpc to collect the window state
  # and save it in redis


