exports.state = require("/state").alerts.state


exports.flash = (message,type="error") ->
  html = ss.tmpl["alerts-message"].render(
    message: message,
    type: type,
    time: ->
      helpers.timestamp()
  )
  $("#mesagebar").empty()
  $(html).appendTo("#messagebar").delay(3000).fadeOut(3000, () ->
    $(this).remove()
  )

exports.redraw = (cb) ->
  # Later on we'll store all messages client-side
  # and redraw the message bar with the list of messages appropriately
  cb && cb()

exports.save = () ->
  # nothing to save really.
