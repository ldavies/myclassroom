# All modules on the client side have a view state
# These view states are property lists that describe how the module looks right now.
# They are stored on the server via the state.js rps and using redis and the state.js middleware
# This file ./state.js will store the common property list description of these view states
#   and will be require()'d by the client and server modules.

exports.chat =
  state:
    display:       false # this should check to see whether auth is true.
    enabled:       true
    subtitle:      "You are not in a chatroom"
    dirty:         true
  transitions:
    "auth.authenticate":
      success:
        display:   true
        enabled:   true
        dirty:     false
      error:
        display:   false
    "auth.logout":
      success:
        display:   false
      error:
        display:   false
  

exports.auth =
  state:
    authenticated: false
    user:          false
    studyGroup:    false
  transitions:
    "auth.authenticate":
      success:
        authenticated: true
      error:
        authenticated: false
    "auth.logout":
      success:
        authenticated: false

exports.alerts =
  state:
    messages:      {}
    errors:        {}

exports.layout =
  state:
    display:       true # not used for now
    dirty:         false

exports.user = 
  state:
    dirty:         false
