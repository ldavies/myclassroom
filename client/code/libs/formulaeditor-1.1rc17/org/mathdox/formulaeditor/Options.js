$package("org.mathdox.formulaeditor.options");

$identify("org/mathdox/formulaeditor/Options.js");

// currently only org.mathdox.formulaeditor.options should be created
// further functions could be placed in an org.mathdox.formulaeditor.options
// record.

// ancientOrbeon: if set to true: do not warn about old orbeon
// dragPalette: if set to true: enable draggable Palette
// fontSize: set font size 
// ignoreTextareaStyle: do not copy options from textarea if set to true
// indentXML: indent created XML
// inputStyle: set default style for Editor Canvases
// paletteShow : default behaviour when showing palettes, choices : 
// - "all" gives a palette if not specified by class
// - "none" gives no palette if not specified by class
// - "one" (default) gives a palette if not specified by class when there
//   is none in the page yet, 
// paletteStyle: set default style for Palette Canvases
// paletteURL: url for palette
// useBar : enable Bar to turn palette on/off

$main(function(){
  // do nothing
});
