# Simple authorization rules
# A grammar that allows simple definition of rules
#  substitute the role name (e.g. student) with the db row of the respective user
# Assumes all db rows with auth rules have an id key

# hierarchy is role -> module -> action
# the where clause for each action is an interpreted SQL lexicon

student:
  user: # viewlist of users
    read:
      where: "user in student.studygroup.users"
    update:
      where: "user = student"
    delete:
      where: "false"
  chatmessage:
    read:
      where: "chatmessage in student.studygroup.chatmessages"
    update:
      where: "false"
    delete:
      where: "false"
