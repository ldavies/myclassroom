
// Define actions which can be called from the client using ss.rpc('app.ACTIONNAME', param1, param2...)
exports.actions = function(req, res, ss) {

  //req.use('debug','cyan');

  // Use session to retrieve userId only 
  req.use('session');

  // Uncomment line below to use the middleware defined in server/middleware/example
  //req.use('auth.authenticated');

  req.use('state.instantiate');

  return {

    save: function(modulename, stateobject)
    {
      return res(false) // save is no longer supported.
    },

    check: function(modulename, stateobject)
    {
      res(req.state.modules[modulename] == stateobject);
    },

    fetch: function(modulename)
    {  
      if (req.state.modules[modulename])
        res(true,req.state.modules[modulename]);
      else {
        try {
          req.state.init(function() {
            res(true,req.state.modules[modulename]);
          });
        } catch (e) {
          err = "Fetch error: state could not be found for " + modulename + ", error: "+ e;
          console.log(err);
          res (false,err);
        }
      }
    },
  };
}
