// Server-side Code
//db = require ("../../db");


// Define actions which can be called from the client using ss.rpc('app.ACTIONNAME', param1, param2...)
exports.actions = function(req, res, ss) {
  req.use('debug','cyan');
  // Example of pre-loading sessions into req.session using internal middleware
  req.use('session');

  req.use('state.instantiate');

  // helper
  var getCurrentUser = function(cb) {
    User.find(req.session.userId)
      .success(cb)
      .error(function(err) {res(false);});
  };

  return {

    isAuthenticated: function(){
      // dodgy. Can't seem to get it to work from the middleware, so repeat the auth check here.
        console.log(req.session);
      if (req.session && (req.session.userId != null)) {
        getCurrentUser(function(user){
       	  res(true, user);
        });
      } else {
        return res(false);
      }
    },

    authenticate: function(email, password){
      var doErr = function (err){
        if (err) console.log(err);
        req.state.error(function() {
          res(false,err);
        });
      };

      // lookup user in DB
      Userauth.find({where:{username:email, password:password}}).success(function(userauth) {
        if (userauth) {
          userauth.getUser().success(function(user) {
            console.log("logged in with user id " + user.id); 
            req.session.setUserId(user.id); 
            req.state.setIdentifier(user.id);
            req.state.success(function() {
              res(true, user);
            });
          }).error(function(err){
            res(false,err);
          });
        } else {
          res(false,"Incorrect username or password");
        }
      }).error(function(err) {
        //doErr(err);
        res(false,"Incorrect username or password");
      });
    },

    logout: function(){
      req.session.setUserId(null);
      req.state.success(function() {
        res(true);
      });
    },
  };
};
