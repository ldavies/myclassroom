// Server-side Code
var _ = require('underscore')._

// Define actions which can be called from the client using ss.rpc('app.ACTIONNAME', param1, param2...)
exports.actions = function(req, res, ss) {

  // Example of pre-loading sessions into req.session using internal middleware
  req.use('session');

  // Uncomment line below to use the middleware defined in server/middleware/example
  req.use('auth.authenticated');

  // every action has a state machine event list to be executed
  req.use('state.instantiate');

  return {

    something: function(){
      return res("something");
    },

    needMessageRefresh: function(latest){
      // select the latest message for this user
      var ch = new Sequelize.Utils.QueryChainer;
      User.find(req.session.userId).success(function(u){
        u.getStudygroup().success(function(g) {
          g.getChatmessages({where: 'id > ' + latest.id}) // if we get none back, we're in sync
           .success(function(m) {
             if (m.length) res(false);
             else res(true);
           })
           .error(function(err) {
             if (!err) res(true);
             else {
               console.log(err);
               res(false);
             }
           });
        }); // error case?
      }); // error case?
    },

    getRecentMessages: function(){
      // select the latest message for this user
      var ch = new Sequelize.Utils.QueryChainer;
      User.find(req.session.userId).success(function(u){
        u.getStudygroup().success(function(g) {
          d = new Date();
          d.addHours(-2);
          g.getChatmessages({where: 'createdAt > "'+ d.toString("yyyy-MM-dd HH:mm:ss")+'"'})
           .success(function(m) {
             if (m.length) res(true,m);
             else res(false);
           })
           .error(function(err) {
             console.log(err);
             res(false);
           });
        }); // error case?
      }); // error case?
    },

    sendMessage: function(message) {
      if (message && message.length > 0) {         // Check for blank messages
        User.find(req.session.userId)
		    .success(function(user) {
          if (user) {
            user.getStudygroup().success(function(associatedStudygroup) {
              if (associatedStudygroup) {
				var cm = Chatmessage.build(
                           {
                             body:message,
                             activated:true,
                             //studygroupId:associatedStudygroup.id,
                             //userId:user.id // should only need to pass object!
                           });
        		var ch2 = new Sequelize.Utils.QueryChainer;
				ch2//.add(cm,'save')
				   .add(cm,'setStudygroup',[associatedStudygroup])
				   .add(cm,'setCreatedBy',[user]) //Doesn't work yet
				   .add(cm,'save')
	               .runSerially()
				   .success(function() {
			         associatedStudygroup
                       .getUsers({where:{chatenabled: true}})
                       .success(function(associatedUsers) {
                       var ids = _.map(associatedUsers,function(e) {return e.id});
                       ss.publish.user(
                         ids,
                         'newMessage', cm);
                       
                       req.state.success(); // we sent a message
                       res(true);
                       })
                        .error(function(err){
                         console.log("There was an error in sending the message: " + err);
                         req.state.error(); // we sent a message
                         res(false, err);
                         });
					});
              } else {
                // Here is where we land if the user is not in a studygroup
                ss.publish.user(req.session.userId,'notInStudygroup');
              }
            }).error(function(err){console.log(err);});
          } else {
            // handle yet another possible error case
            console.log('didnt do shit');
          }
        });
      } else {
        return res(false);
      }
    }

  };
};
