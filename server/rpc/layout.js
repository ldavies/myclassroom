// Server-side Code
var _ = require('underscore')._

// Define actions which can be called from the client using ss.rpc('app.ACTIONNAME', param1, param2...)
exports.actions = function(req, res, ss) {

  // Example of pre-loading sessions into req.session using internal middleware
  req.use('session');

  // Uncomment line below to use the middleware defined in server/middleware/example
  req.use('auth.authenticated');

  return {

	setFullscreen: function(isFullscreen) {
	  User.find(req.session.userId)
	      .success(function(user) {
	        user.isfullscreen = false;
	        user.save().success(function() {
	          // do something here to alert teacher
			  res(true);
            }).error(function(err){res(false);console.log(err)});
          }).error(function(err){res(false);console.log(err)});
	},
	      
  };
};
