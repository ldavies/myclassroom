// Example request middleware

// Only let a request through if the session has been authenticated
// TODO all requests should have success and error terms
// figure out how to do this so that authentication errors will always be handled at the client
exports.authenticated = function() {
  return function(req, res, next) {
    if (req.session && (req.session.userId != null)) {
      console.log("Authenticated!");
      return next();
    } else {
      console.log("Not logged in :(");
      return res(false);
    }
  };
};
