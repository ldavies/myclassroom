module.exports.instantiate = function() {
  return function(req, res, next) {
    if (!req.session || (req.session.userId == null)) {
      return res(false); // no access
    }
    else {
      // find the right state in redis
      req.authorizer = new Authorizer(req.session.userId,req.method);
      return next()
    }
  };
};
