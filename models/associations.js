Classroom.belongsTo(User, {as: "createdBy"})
         .hasMany(User);

User.hasMany(Classroom)
    .belongsTo(Studygroup);

Userauth.belongsTo(User); // user could possibly have many auth rows, but we'll limit this to one row in code.

Studygroup.belongsTo(Classroom) // not null
          .hasMany(User)
          .hasMany(Chatmessage);

Chatmessage.belongsTo(User, {as: 'createdBy'})
           .belongsTo(Studygroup);

Textselection.belongsTo(Textpassage);
