module.exports = function(sequelize,DataTypes) {
	return sequelize.define('Chatmessage', {
	    body:         DataTypes.TEXT,
	    activated:    DataTypes.BOOLEAN
	});
};
