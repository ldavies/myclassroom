module.exports = function(sequelize,DataTypes) {
	return sequelize.define('Classroom', {
	    displayName:  DataTypes.STRING,
	    activated:    DataTypes.BOOLEAN
	});
};

