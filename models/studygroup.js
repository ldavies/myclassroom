module.exports = function(sequelize,DataTypes) {
	return sequelize.define('Studygroup', {
	    displayName:  DataTypes.STRING,
	    activated:    DataTypes.BOOLEAN
	});
};
