module.exports = function(sequelize,DataTypes) {
	return sequelize.define('Textpassage', {
	    title:  DataTypes.STRING,
	    body:    DataTypes.TEXT
	});
};
