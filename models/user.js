module.exports = function(sequelize,DataTypes) {
	return sequelize.define('User', {
	    displayName:  DataTypes.STRING,
	    email:        DataTypes.STRING,
	    activated:    DataTypes.BOOLEAN,
	    chatenabled:  DataTypes.BOOLEAN,
	    isfullscreen: DataTypes.BOOLEAN,
	    role:         DataTypes.INTEGER // 1 = student, 2 = teacher
	});
};
