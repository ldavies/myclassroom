var _ = require('underscore')._;

ModuleState = function(name,defaults) {
  this.name = name;
  this.transitions = {};
  this.state = defaults; // should be a {}
};

// make a state object
// type is 'success' or 'error'
ModuleState.prototype.addTransition = function(req_method,type,params) {
  if (type != 'success' && type != 'fail') throw "Unsupported transition type" + type;
  if (!this.transitions[req_method]) this.transitions[req_method] = {success:{},fail:{}};
  this.transitions[req_method][type] = params;
}


// To run the transition without overwriting the 
// module state (perhaps when using a global 
//  ModuleState instance) pass false for overwrite 
//  and the state parameters for oldparams
ModuleState.prototype.runTransition = function(req_method,type,overwrite,oldparams) {
  // run the transition for this rpc name
  if (!this.transitions[req_method] || !this.transitions[req_method][type]) {
    console.log("No transitions for "+this.name+": "+req_method+": "+type);
    //return false;
    return (overwrite) ? this.state : oldparams;
  }
  if (overwrite) {
    return _.extend(this.state,this.transitions[req_method][type]);
  } else {
    return _.extend({},oldparams,this.transitions[req_method][type]);
  }
};


// now apply to the state defaults
var state_defaults = require(__dirname+'/client/code/app/state');

global.ModuleStates = [];

// add transition objects to the defaults
_.each(state_defaults,function(e,i) {
  global.ModuleStates[i] = new ModuleState(i,e.state);
  _.each(e.transitions,function(e2,i2) {
    global.ModuleStates[i].addTransition(e2.req_method,e2.type,e2.properties);
  });
});


// create a prototype for the state object
