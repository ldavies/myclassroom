var db = require('./db');

seqInstance.sync({force:true})
           .success(function() {	
	var chainer = new Sequelize.Utils.QueryChainer;
	testuser = User.build(
				{
					displayName:"Laurence Davies"
					,email:"lsdavies@mailc.net"
					,activated:true
					,chatenabled:true
					,isfullscreen:false
					,role:1
				});
	testuserauth = Userauth.build(
				{
					username:"lsdavies@mailc.net"
					,password:"password"
				});
	testuser2 = User.build(
				{
					displayName:"Jace Davies"
					,email:"jace@mailc.net"
					,activated:true
					,chatenabled:true
					,isfullscreen:false
					,role:1
				});
	testuserauth2 = Userauth.build(
				{
					username:"jace@mailc.net"
					,password:"password"
				});
	testuser3 = User.build(
				{
					displayName:"Rob Davies"
					,email:"robd@macbackup.com.au"
					,activated:true
					,chatenabled:true
					,isfullscreen:false
					,role:1
				});
	testuserauth3 = Userauth.build(
				{
					username:"robd@macbackup.com.au"
					,password:"password"
				});
	teststudygroup = Studygroup.build(
				{
					displayName:"Test group 1"
					,activated:true
				});
	chainer.add(testuser,'save')
	       .add(testuserauth,'setUser',[testuser])
	       .add(testuserauth,'save')
	       .add(teststudygroup,'save')
	       .add(testuser,'setStudygroup',[teststudygroup])
	       .add(testuser,'save')
	       .add(testuser2,'save')
	       .add(testuserauth2,'setUser',[testuser2])
	       .add(testuserauth2,'save')
	       .add(testuser2,'setStudygroup',[teststudygroup])
	       .add(testuser2,'save')
	       .add(testuser3,'save')
	       .add(testuserauth3,'setUser',[testuser3])
	       .add(testuserauth3,'save')
	       .add(testuser3,'setStudygroup',[teststudygroup])
	       .add(testuser3,'save')
	       .runSerially({skipOnError:true})
	       .success(function() {
				testuserauth.getUser()
	            	.success(function(user) {
	            		console.log(user);
	            	})
	            	.error(function(err){
	            		console.log('did not set user. error: '+ err);
	            	});
				});
	})
	.error(function(error) {
		// shit
		console.log(error);
	});
