myclassroom
===========

myclassroom interactive presentation and feedback environment

####Installation

First, install MySQL server, NodeJS, Redis and Git. Then run the following from the console:

    git clone https://github.com/alphabetsoup/myclassroom.git
    cd myclassroom
    npm install

Create a new database called myclassroom_dev in mysql OR change the database adapter in ./db.js to suit your SQL db.Then run:

    node dbmigrate.js

Now start the server!

    node app.js

Your app should be visible at http://localhost:3000
