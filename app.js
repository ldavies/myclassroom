// My SocketStream 0.3 app

var http          = require('http'),
    ss            = require('socketstream'),
    consoleServer = require('ss-console')(ss);

require("datejs");

consoleServer.listen(5000);

// see if this works
require('./db');
var config = require('./config.json');
seqInstance.sync();

var jslibs = [
'libs/ajquery.min.js'
,'libs/bjquery-ui.min.js'
,'libs/cjquery.bgiframe-2.1.2.js'
,'libs/jlayout.border.js'
,'libs/jlayout.flexgrid.js'
,'libs/jlayout.flow.js'
,'libs/jlayout.grid.js'
,'libs/jquery.fullscreen.js'
,'libs/jquery.jlayout.js'
,'libs/jquery.sizes.js'
//,'libs/formulaeditor-1.1rc17/org/mathdox/formulaeditor/main.js'
,'libs/underscore-min.js'
]
// Define a single-page client called 'main' which is effectively [www.]myclassroom.net.au
ss.client.define('main', {
  view: 'app.html',
  css:  ['libs', 'app.styl'],
  code: jslibs.concat(['app']),
  tmpl: '*'
});

ss.client.define('student', {
  view: 'student.html',
  css:  ['libs/reset.css', 'app.styl'],
  code: ['libs/jquery.min.js', 'app'],
  tmpl: ['common', 'student']
});

ss.client.define('teacher', {
  view: 'teacher.html',
  css:  ['libs/reset.css', 'app.styl'],
  code: ['libs/jquery.min.js', 'app'],
  tmpl: ['common', 'teacher']
});

ss.client.define('staffroom', {
  view: 'staffroom.html',
  css:  ['libs/reset.css', 'app.styl'],
  code: ['libs/jquery.min.js', 'app'],
  tmpl: ['common', 'staffroom']
});

ss.client.define('whiteboard', {
  view: 'whiteboard.html',
  css:  ['libs/reset.css', 'app.styl'],
  code: ['libs/jquery.min.js', 'app'],
  tmpl: ['common', 'whiteboard']
});

// Serve this client on the root URL
ss.http.route('/', function(req, res){
  res.serveClient('main');
});


if (config.useredis) {
	ss.session.store.use('redis');
	ss.publish.transport.use('redis');
	ss.session.options.maxAge = 8640000; // one day 
}

// TODO add a config key/value for the type of transport used.
if (config.transport == 'engine') {
  ss.ws.transport.use(require('ss-engine.io'), {client:{host:'localhost', port:config.server.port}});
}

// Code Formatters
ss.client.formatters.add(require('ss-stylus'));
ss.client.formatters.add(require('ss-coffee'));

// STATE MACHINES
// For now, make these global. TODO use ss.api.add() and fetch through ss object in middleware
//require('./state.js');
var states = require('redis-state-machine');
states.init(__dirname+'/client/code/app/state');
states.setDebug(true);

// Use server-side compiled Hogan (Mustache) templates. Others engines available
ss.client.templateEngine.use(require('ss-hogan'));

// Minimize and pack assets if you type: SS_ENV=production node app.js
if (ss.env === 'production') ss.client.packAssets();

// Start web server
var server = http.Server(ss.http.middleware);
server.listen(config.server.port);

// Start SocketStream
ss.start(server);
